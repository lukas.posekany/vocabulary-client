import { Observable, throwError } from 'rxjs'
import { map, catchError } from 'rxjs/operators'
import { User } from '../interfaces'
import { header } from './header.api'

/** if there was any error with API request
* @param error error returned from API request
*/
function handleError(error: any) {
  if (!error.error || error.error instanceof ErrorEvent) { // client side or network
    error.error = error
    console.error('An error occurred:', error.error);
  } else { // error returned by BE
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
  }
  return throwError(`${error.error}`);  // return observable error
}

/** login user API request
 * @param nameEmail name or email of user
 * @param password password of user
 */
export function loginUserApi(nameEmail: string, password: string): Observable<User> {
  const url = `${process.env.REACT_APP_API_HOST}/authentication/login`
  const body = new URLSearchParams({
    'nameEmail': nameEmail,
    'password': password
  })
  return new Observable<User>(observer => {
    fetch(
      url,
      {
        method: "POST",
        credentials: 'include',
        headers: header,
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.json())
      .then((data: User) => {
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  })
    .pipe(catchError(handleError))
    .pipe(map((data: User) => {
      if (!data.id || !data.role || !data.name || !data.email || !data.words || !data.groups || !data.favorite || !data.favoriteGroups) {
        throwError('not corret response')
      }
      if (data.role !== 'user' && data.role !== 'admin') {
        throwError('not corret response')
      }
      return data
    }));
}

export function setElementApi(userID: string, set: string, element: string, deleteStr: string): Observable<string> {
  const url = `${process.env.REACT_APP_API_HOST}/users/updateUserArray`
  const body = new URLSearchParams({
    "userID": userID,
    "set": set,
    "element": element,
    "delete": deleteStr
  })
  return new Observable<string>(observer => {
    fetch(
      url,
      {
        method: "POST",
        credentials: 'include',
        headers: header,
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.text())
      .then((data: string) => {
        if (data !== "OK") {
          console.error(data)
          throwError('not corret response: ' + data)
        }
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  })
    .pipe(catchError(handleError))
}

/** login user API request
* @param nameEmail name or email of user
* @param password password of user
*/
export function logoutUserApi(): Observable<any> {
  const url = `${process.env.REACT_APP_API_HOST}/authentication/logout`
  return new Observable(observer => {
    fetch(
      url,
      {
        method: "POST",
        headers: header,
        credentials: 'include',
      })
      .then((r: any) => r.text())
      .then((data: string) => {
        if (data === "OK") {
          observer.next("OK")
          observer.complete()
        } else {
          throwError('not OK')
        }
      }).catch((e: any) => {
        throwError("Unable to catch: ", e)
      })
  })
}

/** login user API request
* @param nameEmail name or email of user
* @param password password of user
*/
export function getCurrentUserApi(): Observable<User> {
  const url = `${process.env.REACT_APP_API_HOST}/users/getCurrentUser`
  return new Observable<User>(observer => {
    fetch(
      url,
      {
        method: "POST",
        credentials: 'include',
        headers: header
      })
      .then((r: any) => r.json())
      .then((data: any) => {
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  }).pipe(catchError(handleError))
    .pipe(map((data: User) => {
      if (!data.id || !data.role || !data.name || !data.email || !data.words || !data.groups || !data.favorite || !data.favoriteGroups) {
        throwError('not corret response')
      }
      if (data.role !== 'user' && data.role !== 'admin') {
        throwError('not corret response')
      }
      return data
    }));
}

export function editUserApi(U: User): Observable<string> {
  const url = `${process.env.REACT_APP_API_HOST}/admin/editUser`
  const body = JSON.stringify(U)
  return new Observable<string>(observer => {
    fetch(
      url,
      {
        method: "POST",
        credentials: 'include',
        headers: header,
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.text())
      .then((data: string) => {
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  })

}