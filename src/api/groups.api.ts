import { header } from './header.api'
import {Group} from "../interfaces";
import {Observable} from 'rxjs'

export function fetchGroupsApi(groupsIDs: string[]): Observable<Group[]> {
  if (!groupsIDs || groupsIDs.length === 0) {
    return new Observable(observer => {
      observer.next([]);
      observer.complete();
    })
  }
  const url = `${process.env.REACT_APP_API_HOST}/groups/getGroups`
  const body = new URLSearchParams({
    groupsIDs: `["${groupsIDs.join('","')}"]`
  })
  return new Observable(observer => {
    fetch(
      url,
      {
        method: "POST",
        headers: header,
        credentials: 'include',
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.json())
      .then((data: any) => {
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
  })
}


export function toggleInGroupApi(groupID: string, wordsID: string, add: string): Observable<string> {
  const url = `${process.env.REACT_APP_API_HOST}/groups/addToGroup`
  const body = new URLSearchParams({
    "wordsID": wordsID,
    "groupID": groupID,
    "add": add
  })
  return new Observable(observer => {
    fetch(
      url,
      {
        method: "POST",
        headers: header,
        credentials: 'include',
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.text())
      .then((data: any) => {
        if(data !== "OK") {
          console.error('unable to process: ' + data)
          observer.error('unable to process: ' + data)
        }
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
  })
}

export function addGroupApi(G: Group): Observable<string> {
  const url = `${process.env.REACT_APP_API_HOST}/groups/editGroups`
  const body = JSON.stringify(G)
  return new Observable(observer => {
    fetch(
      url,
      {
        method: "POST",
        headers: header,
        credentials: 'include',
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.text())
      .then((data: any) => {
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  })
}