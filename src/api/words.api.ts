import { header } from './header.api'
import {Observable} from 'rxjs'
import {Words} from "../interfaces";

export function fetchWordsApi(wordsIDs: string[]): Observable<Words[]> {
  if (!wordsIDs || wordsIDs.length === 0) {
    return new Observable(observer => {
      observer.next([]);
      observer.complete();
    })
  }
  const url = `${process.env.REACT_APP_API_HOST}/words/getWords`
  const body = new URLSearchParams({
    wordsIDs: `["${wordsIDs.join('","')}"]`
  })
  return new Observable(observer => {
    fetch(
      url,
      {
        method: "POST",
        headers: header,
        credentials: 'include',
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.json())
      .then((data: any) => {
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  })
}

// return in observable new added id
export function addWordsApi(W: Words): Observable<string> {
  const url = `${process.env.REACT_APP_API_HOST}/words/editWords`
  const body = JSON.stringify(W)
  return new Observable(observer => {
    fetch(
      url,
      {
        method: "POST",
        headers: header,
        credentials: 'include',
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.text())
      .then((data: any) => {
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  })
}

// return in observable new added id
export function deleteWordsApi(wordsID: string): Observable<string> {
  const url = `${process.env.REACT_APP_API_HOST}/words/deleteWords`
  const body = new URLSearchParams({
    wordsID
  })
  return new Observable(observer => {
    fetch(
      url,
      {
        method: "POST",
        headers: header,
        credentials: 'include',
        body: body// body data type must match "Content-Type" header
      })
      .then((r: any) => r.text())
      .then((data: any) => {
        if(data !== "OK") {
          observer.error(data)
        }
        observer.next(data);
        observer.complete();
      }).catch((e: any) => {
        observer.error(e);
      })
    return () => {
      // clean up on unsubscribe
    }
  })
}
