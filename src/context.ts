import React from 'react'
import {RouterService, UserService, WordsService, GroupsService, ServiceContextInterface} from './services'

const routerService = new RouterService()
const userService = new UserService(routerService)
const groupsService = new GroupsService(userService)
const wordsService = new WordsService(userService)

export const servicesGroup: ServiceContextInterface = {
  userService, routerService, groupsService, wordsService
}
export const ServiceContext = React.createContext<ServiceContextInterface>(servicesGroup)