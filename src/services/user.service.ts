import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../interfaces'
import { setStorage } from './storage'
import RouterService from './router.service'
import { loginUserApi, logoutUserApi, setElementApi, getCurrentUserApi } from '../api/index'

export default class UserService {
  userSubject: BehaviorSubject<User> = new BehaviorSubject({} as User)

  constructor(private routerService: RouterService) {
    try {
      this.getCurrentUser().subscribe(
        (user: User) => {
          this.updateUser(user)
          if (window.location.pathname === "/login") {
            this.routerService.routeTo("/")
          }
        },
        error => {
          this.updateUser({} as User)
          this.routerService.routeTo("/login")
          console.error(error)
        }
      )
    } catch (err) {
      console.error(err)
      this.userSubject.next({} as User)
    }
  }

  addWords(wordsId: string): Observable<any> {
    const userId = this.userSubject.value.id
    return new Observable(observer => {
      setElementApi(userId, 'words', wordsId, 'false').subscribe(
        () => {
          const user = this.userSubject.value
          user.words.push(wordsId)
          this.userSubject.next(user)
          observer.next()
          observer.complete()
        },
        (err: Error) => observer.error(err)
      )
    })
  }

  deleteWords(wordsId: string): Observable<any> {
    const userId = this.userSubject.value.id
    const isFavorite = this.userSubject.value.favorite.indexOf(wordsId) >= 0
    return new Observable(observer => {
      if (isFavorite) {
        console.log("IS FAVORITE")
        this.toggleFavorite(wordsId).subscribe(
          () => {
            this.removeFromUser(userId, wordsId).subscribe(
              () => {
                observer.next()
                observer.complete()
              },
              (err) => observer.error(err)
            )
          },
          (err) => observer.error(err)
        )
      } else {
        this.removeFromUser(userId, wordsId).subscribe(
          () => {
            observer.next()
            observer.complete()
          },
          (err) => observer.error(err)
        )
      }
    })
  }

  removeFromUser(userId: string, wordsId: string): Observable<any> {
    return new Observable(observer => {
      setElementApi(userId, 'words', wordsId, 'true').subscribe(
        () => {
          const user = this.userSubject.value
          const index = user.words.indexOf(wordsId)
          user.words.splice(index, 1)
          this.userSubject.next(user)
          observer.next()
          observer.complete()
        },
        (err: Error) => observer.error(err)
      )
    })
  }

  addGroup(groupId: string): Observable<any> {
    const userId = this.userSubject.value.id
    return new Observable(observer => {
      setElementApi(userId, 'groups', groupId, 'false').subscribe(
        () => {
          const user = this.userSubject.value
          user.groups.push(groupId)
          this.userSubject.next(user)
          observer.next()
          observer.complete()
        },
        (err: Error) => observer.error(err)
      )
    })
  }

  updateUser(user: User) {
    setStorage(user)
    this.userSubject.next(user);
  }

  toggleFavorite(wordsId: string): Observable<any> {
    let user: User = this.userSubject.value
    let deleteStr = false
    if (user.favorite.indexOf(wordsId) >= 0) {
      deleteStr = true
    }
    return new Observable(observer => {
      setElementApi(user.id, "favorite", wordsId, deleteStr.toString()).subscribe(
        (data) => {
          if (deleteStr) {
            user.favorite.splice(user.favorite.indexOf(wordsId), 1)
          } else {
            user.favorite.push(wordsId)
          }
          this.userSubject.next(user)
          observer.next()
          observer.complete()
        },
        err => {
          console.error(err)
          observer.error(err)
        }
      )
    })

  }

  loginUser(nameEmail: string, password: string): Observable<User> {
    return new Observable(observer => {
      loginUserApi(nameEmail, password).subscribe(
        (data: User) => {
          this.updateUser(data)
          observer.next(data)
          observer.complete()
        },
        err => {
          observer.error(err)
        },
      )
    })
  }

  logoutUser(): Observable<boolean> {
    return new Observable(observer => {
      logoutUserApi().subscribe(
        () => {
          this.updateUser({} as User)
          this.routerService.routeTo('/login')
          observer.next(true)
          observer.complete()
        },
        (err) => {
          observer.next(false)
          console.error(err)
          observer.complete()
        }
      )
    })
  }

  /** login user API request */
  getCurrentUser(): Observable<User> {
    return new Observable(observer => {
      getCurrentUserApi().subscribe(
        (data: User) => {
          this.updateUser(data)
          observer.next(data)
          observer.complete()
        },
        err => {
          observer.error(err)
        },
      )
    })
  }

  checkUserGuard(): boolean {
    if (this.userSubject.value.role === 'user' || this.userSubject.value.role === 'admin') {
      return true
    }
    return false
  }

  checkAdminGuard(): boolean {
    if (this.userSubject.value.role === 'admin') {
      return true
    }
    return false
  }
}
