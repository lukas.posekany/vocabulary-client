import RouterService from './router.service'
import WordsService from './words.service'
import GroupsService from './groups.service'
import UserService from './user.service'

export interface ServiceContextInterface {
  routerService: RouterService;
  wordsService: WordsService;
  groupsService: GroupsService;
  userService: UserService;
}

export { RouterService, WordsService, GroupsService, UserService}