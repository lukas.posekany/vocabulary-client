import { Words, User } from "../interfaces";
import { UserService } from './index'
import { fetchWordsApi, addWordsApi, deleteWordsApi } from '../api/index'
import { BehaviorSubject, Observable } from 'rxjs';

export default class WordsService {
  wordsSubject: BehaviorSubject<Words[]> = new BehaviorSubject<Words[]>([])

  currentWordsSubject: BehaviorSubject<Words> = new BehaviorSubject<Words>({} as Words)
  constructor(private userService: UserService) {
    this.userService.userSubject.subscribe((user: User) => {
      fetchWordsApi(user.words).subscribe((words: Words[]) => {
        this.wordsSubject.next(words)
      })
    })
  }

  setCurrentWords(w: Words) {
    this.currentWordsSubject.next(w)
  }

  createNewWords(W: Words) {
    addWordsApi(W).subscribe(
      (wordsId: string) => {
        this.userService.addWords(wordsId).subscribe(
          () => {
            W.id = wordsId
            let words = this.wordsSubject.value
            words.push(W)
            this.wordsSubject.next(words)
            this.currentWordsSubject.next(W)
          },
          (err: Error) => console.error(err)
        )
      },
      (err: Error) => console.error(err)
    )
  }

  editWords(W: Words): Observable<any> {
    return new Observable(observer => {
      addWordsApi(W).subscribe(
        (wordsId: string) => {
          if (wordsId !== W.id) {
            observer.error("API didn't returned same id")
          }
          const wordsList: Words[] = this.wordsSubject.value
          const index = wordsList.map((W: Words) => W.id).indexOf(wordsId)
          wordsList[index] = W
          this.wordsSubject.next(wordsList)
          this.currentWordsSubject.next(W)
          observer.next()
          observer.complete()
        },
        (err: Error) => observer.error(err)
      )
    })
  }

  deleteWords() {
    const id = this.currentWordsSubject.value.id
    deleteWordsApi(id).subscribe(
      () => {
        this.userService.deleteWords(id).subscribe(
          () => {
            const words = this.wordsSubject.value
            const index = words.map((W: Words) => W.id).indexOf(id)
            words.splice(index, 1)
            const currentWords = {} as Words
            this.wordsSubject.next(words)
            this.currentWordsSubject.next(currentWords)
          },
          (err) => { console.error(err) }
        )
      },
      (err) => console.error(err)
    )
  }
}


  // loadWords = () => {
  //   fetch('vocabulary.json', {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json'
  //     }
  //   })
  //     .then((response) => {
  //       return response.json();
  //     })
  //     .then((wordsJson: Words[]) => {
  //       this.setState({ wordsList: wordsJson.map((w: Words) => w) })
  //       this.loadGroups()
  //     });
  // }

    // /** set current words set which are bubbled to other components 
  //  * @param w words set satisfying Words interface */
  // setCurrentWords = (w: Words) => {
  //   this.setState({ currentWords: w })
  //   history.push('/game')
  // }