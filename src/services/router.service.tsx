import {Subject} from 'rxjs'

export default class RouterService {
  
  routeSubject: Subject<string> = new Subject()

  routeTo(route: string) {
    if (window.location.pathname !== route) {
      this.routeSubject.next(route)
    }
  }
}