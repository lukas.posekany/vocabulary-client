import { Group, User } from "../interfaces"
import { UserService } from './index'
import { BehaviorSubject} from 'rxjs'
import { fetchGroupsApi, toggleInGroupApi, addGroupApi } from '../api/index'


export default class GroupsService {
  currentGroupSubject: BehaviorSubject<Group> = new BehaviorSubject<Group>({} as Group)
  groupsSubject: BehaviorSubject<Group[]> = new BehaviorSubject<Group[]>([])

  favoriteGroups: BehaviorSubject<Group[]> = new BehaviorSubject([{} as Group])

  constructor(private userService: UserService) {
    this.userService.userSubject.subscribe((user: User) => {
      fetchGroupsApi(user.groups).subscribe((groups: Group[]) => {
        this.groupsSubject.next(groups)
      })
    })
  }

  createNewGroup(G: Group) {
      addGroupApi(G).subscribe(
        (groupId: string) => {
          this.userService.addGroup(groupId).subscribe(
            () => {
              G.id = groupId
              let groups = this.groupsSubject.value
              groups.push(G)
              this.groupsSubject.next(groups)
              this.currentGroupSubject.next(G)
            },
            (err: Error) => console.error(err)
          )
        },
        (err: Error) => console.error(err)
      )
  }

  setCurrentGroup(g: Group) {
    this.currentGroupSubject.next(g)
  }

  toggleInGroup(groupId: string, wordsId: string) {
    let groups: Group[] = this.groupsSubject.value
    const groupIndex = groups.map((G: Group) => G.id).indexOf(groupId)
    if (groupIndex < 0) {
      console.error("OOU, didn't find correct group")
      return
    }
    let add = !(groups[groupIndex].vocabulary.indexOf(wordsId) > -1) // is not in array, shoul be added
    toggleInGroupApi(groupId, wordsId, add.toString()).subscribe(
      () => {
        if (add) { // not there, should add
          groups[groupIndex].vocabulary.push(wordsId)
        } else {   // already contains
          groups[groupIndex].vocabulary.splice(groups[groupIndex].vocabulary.indexOf(wordsId), 1)
        }
        // CHANGE IN GROUP
        this.groupsSubject.next(groups)
      },
      (err) => console.error(err)
    )
  }

}


// export { }


// loadGroups = () => {
//   fetch('groups.json', {
//     headers: {
//       'Content-Type': 'application/json',
//       'Accept': 'application/json'
//     }
//   })
//     .then((response) => {
//       return response.json();
//     })
//     .then((groupsJson: Group[]) => {
//       let groups: Group[] = [
//         this.getGrouOfAll(), this.getGroupOfFavorite()
//       ]
//       for(let group of groupsJson) {
//         groups.push(group)
//       }
//       this.setState({ groupsList: groups })
//     });
// }

// setCurrentGroup = (group: Group) => {
//   let selectedWords: Words[] = [];
//   for(let wID of group.vocabulary) {
//     const words= this.state.wordsList.find(w => w.id === wID)
//     if(words !== undefined) {
//       selectedWords.push(words)
//     }
//   }
//   this.setState({ currentGroup: group, selectedWordsList: selectedWords })
//   history.push('/words')
// }
// getGrouOfAll = (): Group => {
//   let group: Group = {
//     id: 0,
//     name: "alle",
//     icon: "HiDotsCircleHorizontal",
//     vocabulary: []
//   }
//   for (let w of this.state.wordsList) {
//     group.vocabulary.push(w.id)
//   }
//   return group
// }

// getGroupOfFavorite = (): Group => {
//   let group: Group = {
//     id: 1,
//     name: "Lieblings",
//     icon: "AiFillStar",
//     vocabulary: []
//   }
//   for (let w of this.state.wordsList) {
//     if(w.favourite) {
//       group.vocabulary.push(w.id)
//     }
//   }
//   return group
// }