import {User} from '../interfaces'

export function setStorage(user: User) {
  localStorage.setItem('id', user.id);
  localStorage.setItem('name', user.name);
  localStorage.setItem('role', user.role);
  setStorageArray('words', user.words)
  setStorageArray('groups', user.groups)
  setStorageArray('favorite', user.favorite)
  setStorageArray('favoriteGroups', user.favoriteGroups)
}

export function getStorage(): User {
  const user: User = {} as User
  if (localStorage.getItem('id')) { 
    user.id = localStorage.getItem('id') as string
  } else {
    throw new Error('unable to get id');
  }
  if (localStorage.getItem('name')) { 
    user.name = localStorage.getItem('name') as string
  } else {
    throw new Error('unable to get name');
  }
  if (localStorage.getItem('role')) { 
    user.role = localStorage.getItem('role') as string
  } else {
    throw new Error('unable to get role');
  }
  try {
    user.words = getStorageArray('words')
  }
  catch(err) {
    throw err;
  }
  try {
    user.groups = getStorageArray('groups')
  }
  catch(err) {
    throw err;
  }
  try {
    user.favorite = getStorageArray('favorite')
  }
  catch(err) {
    throw err;
  }
  try {
    user.favoriteGroups = getStorageArray('favoriteGroups')
  }
  catch(err) {
    throw err;
  }
  return user
}

export function setStorageArray(name: string, arr: string[]) {
  try {
    localStorage.setItem(name, arr.join(','))
  } catch {
    localStorage.setItem(name, '')
  }
}

export function getStorageArray(name: string): string[] {
  if (typeof localStorage.getItem(name) === 'string') {
    let arr = localStorage.getItem(name)?.split(',')
    if (!arr) {
      arr = []
    }
    return arr
  } else {
    throw new Error(`unable to get ${name}`);
  }
}