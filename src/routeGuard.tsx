import React from 'react';
import { Route, Redirect } from "react-router-dom";
import { ServiceContext } from './context';


interface Props {
  component: typeof React.Component,
  path: string,
}

export const RouteGuard = ({ component: Component, path }: Props) => {
  const context = React.useContext(ServiceContext)
  return (
    <Route exact path={path}
      render={() =>
        context.userService.checkUserGuard() === true
          ? < Component />
          : < Redirect to='/login' />
      } />
  )

}
export const AdminGuard = ({ component: Component, path }: Props) => {
  const context = React.useContext(ServiceContext)
  return (
    <Route exact path={path}
      render={() =>
        context.userService.checkAdminGuard() === true
          ? < Component />
          : < Redirect to='/' />
      } />
  )

}

