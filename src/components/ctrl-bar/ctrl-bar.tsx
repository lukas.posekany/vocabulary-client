import React from 'react'
import './ctrl-bar.scss'
import { CtrlButton } from '../'
import { BsCaretDownFill } from 'react-icons/bs'

export interface ctrlElement {
  text: string
  emit: Function
}

interface Props {
  elements: ctrlElement[]
  activeElement: string
}

interface State {
  display: boolean
}

export class CtrlBar extends React.Component<Props, State> {

  state = {
    display: false
  }

  clickFunction: any;

  componentDidMount() {
    this.clickFunction = this.documentClick
    document.addEventListener("click", this.clickFunction);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.clickFunction);
  }

  documentClick = (e: Event) => {
    const target = e.target as HTMLTextAreaElement
    if (
      target?.id === 'ctrl-bar-list' ||
      target?.parentElement?.id === 'ctrl-bar-list' ||
      target?.parentElement?.parentElement?.id === 'ctrl-bar-list' ||
      target?.id === 'open-bar-icon' ||
      target?.parentElement?.id === 'open-bar-icon' ||
      target?.parentElement?.parentElement?.id === 'open-bar-icon' ||
      target?.parentElement?.parentElement?.parentElement?.id === 'open-bar-icon' ||
      target?.parentElement?.parentElement?.parentElement?.parentElement?.id === 'open-bar-icon'
      ) {
      } else {
        this.setState({ display: false })
      }
    }
    
    renderElements() {
    if (this.props.elements.length === 0) {
      return <li></li>
    }
    return this.props.elements.map((element: ctrlElement, index: number) => {
      return (
        <div 
          key={`${element.text + index}`}
          >
          <CtrlButton
            content={element.text}
            active={element.text===this.props.activeElement}
            click={() => {
              this.setState({display: false})
              element.emit()
            }}
          />
        </div>
      )
    })
  }

  render() {
    let list
    if (this.state.display) {
      list = (
        <div className="list-wrapper">
          <div
            className="ctrl-bar-list"
            id="ctrl-bar-list"
            >
            {this.renderElements()}
          </div>
        </div>
      )
    }
    return (
      <div className="ctrl-bar-component">
        <div
          className="open-bar-icon"
          id="open-bar-icon"
          onClick={() => this.setState({ display: !this.state.display })}>
          <BsCaretDownFill />
        </div>
        {list}
      </div>
    )
  }
}