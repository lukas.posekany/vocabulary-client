import React from 'react'
import { UserService } from '../../services';
import { editUserApi } from '../../api'
import { ServiceContext } from '../../context'
import { User } from '../../interfaces/'
import './edit-user.scss'
interface Props {

}
interface State {
  name: string
  admin: boolean
  email: string
  password: string
}

export class EditUser extends React.Component<Props, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  userService: UserService = {} as UserService

  state = {
    name: '',
    admin: false,
    email: '',
    password: ''
  }

  componentDidMount() {
    this.userService = this.context.userService
  }

  submitForm() {
    const U: User = {} as User
    U.name = this.state.name
    U.email = this.state.email
    U.password = this.state.password
    U.favorite = []
    U.favoriteGroups = []
    U.groups = []
    U.words = []
    if (this.state.admin) {
      U.role = 'admin'
    } else {
      U.role = 'user'
    }
    editUserApi(U).subscribe(
      (id: string) => {
        alert("NEW USER ADDED: " + id)
        this.setState({
          name: '',
          admin: false,
          email: '',
          password: ''
        })
      },
      (err: Error) => { console.error(err) }
    )
  }

  render() {
    return (
      <div className="edit-user">
        <label>Name</label>
        <input
          type="text"
          onChange={(e) => this.setState({ name: e.target.value })}
          value={this.state.name}
          autoCapitalize="off"
          autoComplete="off"
          spellCheck="false"
          autoCorrect="off"
        />
        <label>Email</label>
        <input
          type="text"
          onChange={(e) => this.setState({ email: e.target.value })}
          value={this.state.email}
          autoCapitalize="off"
          autoComplete="off"
          spellCheck="false"
          autoCorrect="off"
        />
        <label>Password</label>
        <input
          type="password"
          onChange={(e) => this.setState({ password: e.target.value })}
          value={this.state.password}
        />
        <input
          id="admin-input"
          type="checkbox"
          checked={this.state.admin}
          onChange={() => this.setState({ admin: !this.state.admin })}
        />
        <label htmlFor="admin-input">Admin</label>
        <button type="button" onClick={() => this.submitForm()}>SUBMIT</button>
      </div>
    )
  }
}