import React from 'react'
import { Words, Group } from '../../interfaces'
import './edit-words.scss'
import { ServiceContext } from '../../context'
import { RouterService, GroupsService, WordsService, UserService } from '../../services';
import { CreateWords } from './create-words/create-words'
import { CreateGroup } from './create-group/create-group'
import { EditExisting } from './edit-existing/edit-existing';
import { ImportWords } from './import-words/import-words';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CtrlBar, ctrlElement } from '../'

interface State {
  currentWords: Words
  groups: Group[]
  display: string
}

export class EditWords extends React.Component<any, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  routerService: RouterService = {} as RouterService
  userService: UserService = {} as UserService
  wordsService: WordsService = {} as WordsService
  groupsService: GroupsService = {} as GroupsService

  destroy$: Subject<boolean> = new Subject<boolean>()

  state = {
    currentWords: {} as Words,
    groups: [],
    display: 'edit'
  }

  ctrlBar: ctrlElement[] = [
    {
      text: "edit words",
      emit: () => this.setState({ display: 'edit words' })
    }, {
      text: "add words",
      emit: () => this.setState({ display: 'add words' })
    }, {
      text: "add group",
      emit: () => this.setState({ display: 'add group' })
    }, {
      text: "import words",
      emit: () => this.setState({ display: 'import words' })
    }
  ]

  componentDidMount() {
    this.routerService = this.context.routerService
    this.wordsService = this.context.wordsService
    this.groupsService = this.context.groupsService
    this.userService = this.context.userService

    this.groupsService.groupsSubject
      .pipe(takeUntil(this.destroy$))
      .subscribe((groups: Group[]) => {
        this.setState({ groups, display: 'edit words' })
      })

    this.wordsService.currentWordsSubject
      .pipe(takeUntil(this.destroy$))
      .subscribe((W: Words) => {
        if (!W || Object.keys(W).length <= 0) {
          this.setState({ display: 'add words' })
        } else {
          this.setState({ display: 'edit words' })
        }
        this.setState({ currentWords: W })
      })
  }

  componentWillUnmount() {
    this.destroy$.next(true)
    this.destroy$.unsubscribe()
  }

  render() {
    let content
    if (this.state.display === 'add words') {
      content = (
        <React.Fragment key={Math.random()}>
          <CreateWords
            wordsService={this.wordsService}
            userService={this.userService}
          />
        </React.Fragment>
      )
    } else if (this.state.display === 'add group') {
      content = (
        <React.Fragment key={Math.random()}>
          <CreateGroup
            groupsService={this.groupsService}
            userService={this.userService}
          />
        </React.Fragment>
      )
    } else if (this.state.display === 'import words') {
      content = (
        <ImportWords
          wordsService={this.wordsService}
        />
      )
    } else {
      content = (
        <EditExisting key={Math.random()}
          currentWords={this.state.currentWords}
          groups={this.state.groups}
        />
      )
    }


    return (
      <div className="edit-words">
        <CtrlBar
        key="ctrl-bar"
          elements={this.ctrlBar}
          activeElement={this.state.display}
        />
        <div className="edit-words-content">
          {content}
        </div>
      </div>
    )
  }
}