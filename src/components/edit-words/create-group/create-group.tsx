import React from 'react'
import { Group } from '../../../interfaces'
import './create-group.scss'
import { UserService, GroupsService } from '../../../services'
import { IconsPicker } from '../../'
import { CtrlButton } from '../../'

interface Props {
  groupsService: GroupsService
  userService: UserService
}

interface State {
  selectedIcon: string
  groupName: string
}

export class CreateGroup extends React.Component<Props, State> {
  state = {
    selectedIcon: '',
    groupName: ''
  }

  submitForm() {
    if (this.state.groupName === '') {
      alert('words should have some name')
      return
    }
    const G: Group = {
      id: '',
      name: this.state.groupName,
      icon: this.state.selectedIcon,
      vocabulary: []
    }
    this.props.groupsService.createNewGroup(G)
  }


  render() {
    return (
      <div className="create-group">
        <label >Group Name</label>
        <input
          type="text"
          id="name"
          onChange={(e) => this.setState({ groupName: e.target.value })}
          value={this.state.groupName}
          autoCapitalize="off"
          autoComplete="off"
          spellCheck="false"
          autoCorrect="off"
        ></input>

        <label >Icon</label>
        <IconsPicker
          selectedIcon={this.state.selectedIcon}
          selectIcon={(icon: string) => { this.setState({ selectedIcon: icon }) }}
        />
        <div className="send-btn">
          <CtrlButton
            content="SEND"
            active={false}
            click={() => this.submitForm()}
          />
        </div>
      </div>
    )
  }
}