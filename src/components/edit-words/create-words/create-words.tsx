import React from 'react'
import { Words } from '../../../interfaces'
import './create-words.scss'
import { IconsPicker } from '../../'
import { UserService, WordsService } from '../../../services'
import { CtrlButton } from '../../'

interface allowedGame {
  name: string
  checked: boolean
}

interface Props {
  wordsService: WordsService
  userService: UserService
}

interface State {
  allowedGames: allowedGame[]
  selectedIcon: string
  wordsName: string
  L1: string
  L2: string
}

export class CreateWords extends React.Component<Props, State> {
  knownGames = [
    {
      name: 'write',
      checked: true
    }, {
      name: 'pair',
      checked: true
    }, {
      name: 'genus',
      checked: false
    }
  ]

  state = {
    allowedGames: this.knownGames,
    selectedIcon: '',
    wordsName: '',
    import: false,
    L1: '',
    L2: ''
  }

  checkGame(name: string) {
    let allowedGames = this.state.allowedGames
    let gameIndex = allowedGames.map((game: allowedGame) => game.name).indexOf(name)
    allowedGames[gameIndex].checked = !allowedGames[gameIndex].checked
    this.setState({ allowedGames })
  }

  submitForm() {
    if (this.state.wordsName === '') {
      alert('words should have some name')
      return
    }
    const W: Words = {
      id: '',
      name: this.state.wordsName,
      group: '',
      icon: this.state.selectedIcon,
      allowedGames: this.state.allowedGames
        .filter((game: allowedGame) => game.checked)
        .map((game: allowedGame) => game.name),
      languages: [this.state.L1, this.state.L2],
      words: []
    }
    this.props.wordsService.createNewWords(W)
  }

  renderAllowedGames() {
    return this.state.allowedGames.map((game: allowedGame) => (
      <li key={game.name}>
        <input
          id={game.name}
          type="checkbox"
          onChange={() => this.checkGame(game.name)}
          checked={game.checked} />
        <label htmlFor={game.name}>{game.name}</label>
      </li>
    ))
  }
  render() {
    return (
      <div className="create-words">
        <label >Name</label>
        <input
          type="text"
          id="name"
          onChange={(e) => this.setState({ wordsName: e.target.value })}
          value={this.state.wordsName}
          autoCapitalize="off"
          autoComplete="off"
          spellCheck="false"
          autoCorrect="off"
        />

        <label >Icon</label>
        <IconsPicker
          selectedIcon={this.state.selectedIcon}
          selectIcon={(icon: string) => { this.setState({ selectedIcon: icon }) }}
        />

        <label >Allowed Games</label>
        <ul>
          {this.renderAllowedGames()}
        </ul>

        <label >Languages</label>
        <input
          type="text"
          id="language1"
          placeholder="L1"
          onChange={(e) => this.setState({ L1: e.target.value })}
          value={this.state.L1}
          autoCapitalize="off"
          autoComplete="off"
          spellCheck="false"
          autoCorrect="off"
        />
        <input
          type="text"
          id="language2"
          placeholder="L2"
          onChange={(e) => this.setState({ L2: e.target.value })}
          value={this.state.L2}
          autoCapitalize="off"
          autoComplete="off"
          spellCheck="false"
          autoCorrect="off"
        />
        <div className="send-btn">
          <CtrlButton
            content="SEND"
            active={false}
            click={() => this.submitForm()}
          />
        </div>
      </div>
    )
  }
}