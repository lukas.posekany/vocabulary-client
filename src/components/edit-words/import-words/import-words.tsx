import React from 'react'
import './import-words.scss'
import { Words } from '../../../interfaces'
import { WordsService } from '../../../services'

interface Props {
  wordsService: WordsService
}

interface State {
  jsonImport: string
}

export class ImportWords extends React.Component<Props, State> {
  state = {
    jsonImport: ''
  }

  submitImport() {
    const string = this.state.jsonImport
    const jsonObject = JSON.parse(string)
    const W = {} as Words
    if (jsonObject.name) {
      W.name = jsonObject.name
    } else {
      alert("missing name")
      return
    }

    if (jsonObject.words) {
      W.words = jsonObject.words
    } else {
      alert("missing words")
      return
    }

    if (jsonObject.languages) {
      W.languages = jsonObject.languages
    } else {
      alert("missing languages")
      return
    }
    
    if (jsonObject.allowedGames) {
      W.allowedGames = jsonObject.allowedGames
    } else {
      alert("missing allowed games")
      return
    }

    if (jsonObject.icon) {
      W.icon = jsonObject.icon
    } else {
      alert("missing icon")
      return
    }
    this.props.wordsService.createNewWords(W)
  }

  render() {
    return (
      <div className="import-words-component">
        <textarea
        rows={20}
          onChange={(e) => this.setState({ jsonImport: e.target.value })}
          value={this.state.jsonImport}
          placeholder="insert words as JSON"
        />
        <button onClick={() => this.submitImport()}>SEND</button>
      </div>
    )
  }
}

// EXAMPLE OF VALIDE JSON
// {
//   "name": "NEUE",
//   "group": "Substantive",
//   "icon": "MdFiberNew",
//   "favourite": true,
//   "allowedGames": ["write", "pair", "genus" ],
//   "languages": ["CZ","DE"],
//   "words": [
//     ["zelí", ""],
//     ["ven", "heraus"],
//     ["málo", "wenig"],
//     ["koule", "e Kugel"],
//     ["lovec", "e Jäger"],
//     ["souhlasím", "genau"],
//     ["rozcestník", "r Wegweiser"],
//     ["směr", "e Richtung"],
//     ["další", "weiter"],
//     ["lékárna", "r Apotheke"],
//     ["otevřeno", "geöffnen"],
//     ["zavřeno", "geschlossen"],
//     ["čistý", "sauber"],
//     ["ošklivý", "hässlich"],
//     ["sprcha", "r Dusche"],
//     ["sladké", "süß"],
//     ["kancelář", "s Büro"]
//   ]
// }