import React from 'react'
import './edit-existing.scss'
import { AiOutlinePlus } from 'react-icons/ai'
import { Words, Group } from '../../../interfaces'
import { BiTrash } from 'react-icons/bi'
import { GroupsService, WordsService } from '../../../services';
import { ServiceContext } from '../../../context'
import { CtrlButton, CheckboxBlock, checkboxElement } from '../../'

interface Props {
  currentWords: Words
  groups: Group[]
}

interface State {
  currentWords: Words
  changes: boolean
  groups: Group[]
  L1: string
  L2: string
}

export class EditExisting extends React.Component<Props, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  wordsService: WordsService = {} as WordsService
  groupsService: GroupsService = {} as GroupsService
  state = {
    currentWords: {} as Words,
    changes: false,
    groups: [],
    L1: "",
    L2: ""
  }

  componentDidMount() {
    this.wordsService = this.context.wordsService
    this.groupsService = this.context.groupsService
    this.setState({
      currentWords: JSON.parse(JSON.stringify(this.props.currentWords)),
      groups: this.props.groups,
    })
  }

  renderGroupCheckbox(): checkboxElement[] {
    return this.state.groups.map((group: Group): checkboxElement => {
      return {
        checked: group.vocabulary.indexOf(this.state.currentWords.id) > -1,
        label: group.name,
        emit: () => this.toggleGroup(group.id)
      }
    })
  }

  toggleGroup(id: string) {
    this.groupsService.toggleInGroup(id, this.state.currentWords.id)
  }

  sendChanges() {
    this.wordsService.editWords(this.state.currentWords).subscribe()
  }

  renderWordsList() {
    if (this.state.currentWords?.words === undefined) {
      return (<React.Fragment></React.Fragment>)
    }
    return this.state.currentWords.words.map((word: string[], i: number) => (
      <li key={`word${i}`}>
        <input
          key={`wordL1${i}`}
          className="word"
          onChange={(e) => this.changeWord(i, 0, e.target.value)}
          value={this.state.currentWords.words[i][0]}
        />
        <input
          key={`wordL2${i}`}
          className="word"
          onChange={(e) => this.changeWord(i, 1, e.target.value)}
          value={this.state.currentWords.words[i][1]}
        />
        <button onClick={() => this.removeWord(i)}><BiTrash /></button>
      </li>
    ))
  }

  changeName(name: string) {
    const words = this.state.currentWords
    words.name = name
    this.setState({ currentWords: words, changes: true })
  }

  changeWord = (index: number, languageIndex: number, value: string) => {
    let words = this.state.currentWords
    words.words[index][languageIndex] = value
    this.setState({ currentWords: words, changes: true })
  }

  addToWords() {
    if (this.state.L1 === "" || this.state.L2 === "") {
      alert("words shouldn't be blank!!!")
      return
    }
    const W = this.state.currentWords
    W.words.push([this.state.L1, this.state.L2])
    this.setState({ L1: "", L2: "", changes: true })
  }

  removeWord(index: number) {
    const W = this.state.currentWords
    W.words.splice(index, 1)
    this.setState({ currentWords: W, changes: true })
  }

  removeWordsSet() {
    var removeName = prompt("Please enter name of words set", '');

    if (removeName === this.state.currentWords.name) {
      console.log('removing set: ' + removeName)
      this.wordsService.deleteWords()
    } else {
      alert('not correct name to delete: ' + removeName)
    }
  }

  render() {
    let sendBTN;
    if (this.state.changes) {
      sendBTN =
        <div className="send-btn">
          <CtrlButton
            content="SEND CHANGES"
            active={false}
            click={() => this.sendChanges()}
          />
        </div>
    }

    let groupsBlock;
    if (this.state.groups.length > 0) {
      groupsBlock = (
        <CheckboxBlock
          label="groups"
          elements={this.renderGroupCheckbox()}
        />
      )
    }

    if (Object.keys(this.state.currentWords).length <= 0) {
      return <div></div>
    }

    return (
      <div className="edit-existing-component">

        <input
          type="text"
          value={this.state.currentWords.name}
          onChange={(e) => this.changeName(e.target.value)}
        />
        <div className="delete-words">
          <CtrlButton
            content="DELETE THIS SET"
            active={false}
            click={() => this.removeWordsSet()}
          />
        </div>

        <div>{groupsBlock}</div>

        <ul className="edit-words-list">
          {this.renderWordsList()}
          <li>
            <input
              type="text"
              value={this.state.L1}
              onChange={e => this.setState({ L1: e.target.value })}
              placeholder="L1"
              autoCapitalize="off"
              autoComplete="off"
              spellCheck="false"
              autoCorrect="off"
            />
            <input
              type="text"
              onChange={e => this.setState({ L2: e.target.value })}
              placeholder="L2"
              value={this.state.L2}
              autoCapitalize="off"
              autoComplete="off"
              spellCheck="false"
              autoCorrect="off"
            />
            <button className="add-words" onClick={() => this.addToWords()}><AiOutlinePlus /></button>
          </li>
        </ul>
        {sendBTN}
      </div>
    )
  }
}