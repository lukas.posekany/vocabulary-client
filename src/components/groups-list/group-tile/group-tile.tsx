import React from 'react'
import {Group} from '../../../interfaces'
import {TileIcon} from '../..'
import './group-tile.scss'

interface Props {
  group: Group
}

export class GroupTile extends React.Component<Props, any> {
  render() {
    if(!this.props.group.vocabulary) {
      return <React.Fragment></React.Fragment>
    }
    return (
      <div className="group-main-tile">
        <div className="group-icon"><TileIcon iconName={this.props.group.icon}/></div>
        <div className="group-info">
          <div className="group-name">{this.props.group.name}</div>
          <div className="group-words">{this.props.group.vocabulary.length} in dieser Gruppe</div>
        </div>
      </div>
    )
  }
}