import React from 'react'
import {Group, User} from '../../interfaces'
import './groups-list.scss'
import { GroupTile } from './group-tile/group-tile'
import { ServiceContext } from '../../context'
import { GroupsService, RouterService, UserService, WordsService } from '../../services'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

interface Props {
}

interface State {
  groupsList: Group[],
  favorite: Group,
  all: Group
}

export class GroupsList extends React.Component<Props, State> {

  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  routerService: RouterService = {} as RouterService
  wordsService: WordsService = {} as WordsService
  groupsService: GroupsService = {} as GroupsService
  userService: UserService = {} as UserService

  destroy$: Subject<boolean> = new Subject<boolean>()

  state = {
    groupsList: [],
    favorite: {} as Group,
    all: {} as Group
  }

  componentDidMount() {
    this.routerService = this.context.routerService
    this.wordsService = this.context.wordsService
    this.groupsService = this.context.groupsService
    this.userService = this.context.userService
    this.userService.userSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((user: User) => {
      const favorite: Group = {
        id: 'aaa',
        name: 'Favorite',
        icon: 'AiFillStar',
        vocabulary: user.favorite
      }
      const all: Group = {
        id: 'bbb',
        name: 'All',
        icon: 'HiDotsCircleHorizontal',
        vocabulary: user.words
      }
      this.setState({favorite: favorite, all: all})
    })
    this.groupsService.groupsSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((groups: Group[]) => {
      this.setState({groupsList: groups})
    })
  }

  componentWillUnmount() {
    this.destroy$.next(true)
    this.destroy$.unsubscribe()
  }

  setGroup(G: Group) {
    this.groupsService.setCurrentGroup(G)
    this.routerService.routeTo('/words')
  }

  renderList = () => {
    if (this.state.groupsList === undefined) {
      // this.routerService.routeTo('/');
      <React.Fragment></React.Fragment>
      return
    }
    return this.state.groupsList.map(
      (g: Group) =>
        <li key={g.name} onClick={() => this.setGroup(g)} >
          <GroupTile group={g} />
        </li>
    )
  }

  render() {
    return (
      <div className="groups-list">
        <ul>
          <li onClick={() => this.setGroup(this.state.favorite)}><GroupTile group={this.state.favorite} /></li>
          <li onClick={() => this.setGroup(this.state.all)}><GroupTile group={this.state.all} /></li>
          {this.renderList()}
        </ul>
      </div>
    )
  }
}