import React from 'react';
import { IoDice } from 'react-icons/io5';
import { FaClipboardList, FaUserGraduate } from 'react-icons/fa';
import { MdEdit } from 'react-icons/md';
import { AiFillHome } from 'react-icons/ai';
import { Words } from '../../interfaces';
import './top-nav.scss'
import { ServiceContext } from '../../context'
import { RouterService, UserService, WordsService } from '../../services';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


interface Props {
}

interface State {
  currentWords: Words
  displayUserCtrl: boolean
}

export class TopNav extends React.Component<Props, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  userService: UserService = {} as UserService
  wordsService: WordsService = {} as WordsService
  routerService: RouterService = {} as RouterService

  destroy$: Subject<boolean> = new Subject<boolean>()

  state = {
    currentWords: {} as Words,
    displayUserCtrl: false
  } as State

  componentDidMount() {
    this.userService = this.context.userService
    this.routerService = this.context.routerService
    this.wordsService = this.context.wordsService
    this.wordsService.currentWordsSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((W: Words) => {
      this.setState({ currentWords: W })
    })
    document.addEventListener("click", (e) => this.documentClick(e));
  }

  componentWillUnmount() {
    document.removeEventListener("click", (e) => this.documentClick(e));
    this.destroy$.next(true)
    this.destroy$.unsubscribe()
  }

  documentClick(e: Event) {
    const target = e.target as HTMLTextAreaElement
    if (
      target?.id === 'user-menu' ||
      target?.parentElement?.id === 'user-menu' ||
      target?.id === "user-menu-btn" ||
      target?.parentElement?.id === 'user-menu-btn' ||
      target?.parentElement?.parentElement?.id === 'user-menu-btn'
    ) {
    } else {
      this.setState({ displayUserCtrl: false })
    }
  }

  logout() {
    this.userService.logoutUser().subscribe()
  }

  render() {
    let userMenu
    if (this.state.displayUserCtrl) {
      let admin = this.userService.userSubject.value.role === 'admin'
      let adminList
      if (admin) {
        adminList = (
          <li onClick={() => this.routerService.routeTo('/editUser')}>CREATE USER</li>
        )
      }
      userMenu = (
        <ul className="user-menu" id="user-menu">
          <li onClick={() => this.logout()}>LOGOUT</li>
          {adminList}
        </ul>
      )
    }
    return (
      <nav className="main-nav">
        <ul className="words-name">
          <li>{this.state.currentWords.name}</li>
        </ul>
        <ul>
          <li onClick={() => this.routerService.routeTo('/groups')}><FaClipboardList /></li>
          <li onClick={() => this.routerService.routeTo('/edit')}><MdEdit /></li>
          <li onClick={() => this.routerService.routeTo('/game')}><IoDice /></li>
          <li onClick={() => this.routerService.routeTo('/')}><AiFillHome /></li>
          {/* EDIT USER SHOULD BE LATER LIST OF POSSIBLE CHOICES (LOGIN, LOGOUT, CREATE, ..) */}
          <li id="user-menu-btn" onClick={() => this.setState({ displayUserCtrl: !this.state.displayUserCtrl })}>
            <FaUserGraduate />
          </li>
        </ul>
        {userMenu}
      </nav>

    )
  }
}