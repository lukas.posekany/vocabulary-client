import React from 'react'
import './home.scss'
import { Words, Group, User } from '../../interfaces'
import {TileIcon} from '../tile-icons/tile-icons'
import { ServiceContext } from '../../context'
import { GroupsService, UserService, WordsService, RouterService } from '../../services'
import { Subject} from 'rxjs'
import { takeUntil } from 'rxjs/operators'

interface State {
  wordsList: Words[];
  groupsList: Group[];
  favWordsList: string[];
  favGroupsList: string[];
}


export class Home extends React.Component<any, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  wordsService: WordsService = {} as WordsService
  groupsService: GroupsService = {} as GroupsService
  userService: UserService = {} as UserService
  routerService: RouterService = {} as RouterService

  destroy$: Subject<boolean> = new Subject<boolean>()

  componentDidMount() {
    this.wordsService = this.context.wordsService
    this.groupsService = this.context.groupsService
    this.userService = this.context.userService
    this.routerService = this.context.routerService
    this.userService.userSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((user: User) => {
      this.setState({
        favWordsList: user.favorite,
        favGroupsList: user.favoriteGroups
      })
    })
    this.wordsService.wordsSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((words: Words[]) => {
      this.setState({ wordsList: words })
    })
    this.groupsService.groupsSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((groups: Group[]) => {
      this.setState({ groupsList: groups })
    })
  }

  componentWillUnmount() {
    this.destroy$.next(true)
    this.destroy$.unsubscribe()
  }

  setGroup = (G: Group) => {
    this.groupsService.setCurrentGroup(G)
    this.routerService.routeTo('words')
  }

  setWords = (W: Words) => {
    this.wordsService.setCurrentWords(W)
    this.routerService.routeTo('game')
  }

  renderFavoriteGroups() {
    if (!this.state?.favGroupsList) { return <span></span> }
    if (this.state.favGroupsList.length === 0) { return <span></span> }
    if (this.state.groupsList === null || this.state.groupsList.length === 0) { return <span></span> }
    return this.state.favGroupsList.map((id: string) => {
      const G = this.state.groupsList.find((g: Group) => g.id === id)
      if (!G) {
        return <span></span>;
      }
      return (
        <li
          key={G.name}
          onClick={() => this.setGroup(G)}>
          <TileIcon iconName={G.icon} />
        </li>)
    }
    )
  }

  renderFavoriteWords() {
    if (!this.state?.favWordsList) { return <span></span> }
    if (this.state?.favWordsList?.length === 0) { return <span></span> }
    if (this.state.wordsList === null || this.state.wordsList.length === 0) { return <span></span> }
    return this.state.favWordsList.map((id: string) => {
      const W = this.state.wordsList.find((w: Words) => w.id === id)
      if (!W) {
        return <span></span>;
      }
      return (
        <li
          key={W.name}
          onClick={() => this.setWords(W)}
        >
          <div className="icon">
            <TileIcon iconName={W.icon} />
          </div>
          <div className="name">
            {W.name}
          </div>
        </li>)
    }
    )
  }

  render() {
    return (
      <div className="home-component">
        <ul className="small-tile">
          {this.renderFavoriteGroups()}
        </ul>
        <ul className="large-tile">
          {this.renderFavoriteWords()}
        </ul>
      </div>
    )
  }
}