import React from 'react'
import './login.scss'
import { ServiceContext } from '../../context'
import { RouterService, UserService } from '../../services';

interface Props {
}

interface State {
  wrongAuth: boolean,
  nameEmail: string,
  password: string
}

export class Login extends React.Component<Props, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  userService: UserService = {} as UserService
  routerService: RouterService = {} as RouterService

  state = {
    wrongAuth: false,
    nameEmail: '',
    password: ''
  }

  componentDidMount() {
    this.userService = this.context.userService
    this.routerService = this.context.routerService
  }

  submit = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault()
    const nameEmail = this.state.nameEmail
    const password = this.state.password
    this.userService.loginUser(nameEmail, password)
      .subscribe(
        () => {
          this.setState({ wrongAuth: false })
          this.routerService.routeTo('/');
        },
        (error: any) => {
          console.error("ERROR", error)
          if (error === 'TypeError: Failed to fetch') {
            this.setState({ wrongAuth: true })
          } else if (error === '401') {
            this.setState({ wrongAuth: true })
          }
        });
  }

  render() {
    return (
      <form className="login-component">
        <label>name or email:</label>
        <input
          type="text"
          name="nameEmail"
          value={this.state.nameEmail}
          onChange={(e) => this.setState({ nameEmail: e.target.value })}
          autoCapitalize="off"
          autoComplete="off"
          spellCheck="false"
          autoCorrect="off"
        />
        <label>password:</label>
        <input
          type="password"
          name="password"
          value={this.state.password}
          onChange={(e) => this.setState({ password: e.target.value })}
        />
        <button type="submit" onClick={this.submit}>LOGIN</button>
        {/* <input type="submit" value="Submit" /> */}
      </form>
    )
  }
}

