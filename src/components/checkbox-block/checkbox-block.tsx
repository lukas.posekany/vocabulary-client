import React from 'react'
import './checkbox-block.scss'
import { BsCaretDownFill } from 'react-icons/bs'

export interface checkboxElement {
  label: string;
  checked: boolean;
  emit: Function;
}

interface Props {
  label: string
  elements: checkboxElement[]
}

interface State {
  display: boolean
  search: string
}

export class CheckboxBlock extends React.Component<Props, State> {

  state = {
    display: false,
    search: ''
  }

  clickFunction: any

  componentDidMount() {
    this.clickFunction = this.documentClick
    document.addEventListener("click", this.clickFunction);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.clickFunction);
  }

  documentClick = (e: Event) => {
    const target = e.target as HTMLTextAreaElement
    if (
      target?.id === 'checkbox-label' ||
      target?.parentElement?.id === 'checkbox-label' ||
      target?.parentElement?.parentElement?.id === 'checkbox-label' ||
      target?.parentElement?.parentElement?.parentElement?.id === 'checkbox-label' ||
      target?.id === 'list-content' ||
      target?.parentElement?.id === 'list-content' ||
      target?.parentElement?.parentElement?.id === 'list-content' ||
      target?.parentElement?.parentElement?.parentElement?.id === 'list-content' ||
      target?.parentElement?.parentElement?.parentElement?.parentElement?.id === 'list-content'
    ) {
    } else {
      this.setState({ display: false })
    }
  }

  renderElements() {
    if (this.props.elements.length === 0) {
      return <div></div>
    }
    return this.props.elements
      .filter((element: checkboxElement) => element.label.includes(this.state.search))
      .map((element: checkboxElement, index: number) => {
        return (
          <div
            key={index}>
            <input
              id={`group${index}`}
              onChange={() => element.emit()}
              type="checkbox"
              checked={element.checked} />
            <label htmlFor={`group${index}`}>{element.label}</label>
          </div>
        )
      })
  }

  render() {
    let list
    if (this.state.display) {
      list = (
        <div className="list-wrapper">
          <div className="list-content"
          id="list-content">
            <input
              type="text"
              className="search-input"
              onChange={(e) => this.setState({ search: e.target.value })}
              value={this.state.search}
              placeholder="search"
            />
            <div
              className="ctrl-checkbox-list"
               >
              {this.renderElements()}
            </div>
          </div>
        </div>
      )
    }
    return (
      <div className="checkbox-block-component">
        <div
          className="label"
          id="checkbox-label"
          onClick={() => this.setState({ display: !this.state.display })}>
          <div className="label-text">
            {this.props.label}
          </div>
          <div className="arrow-icon">
            <BsCaretDownFill />
          </div>
        </div>
        {list}
      </div >
    )
  }
}
