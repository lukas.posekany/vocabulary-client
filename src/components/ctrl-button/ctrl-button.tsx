import React from 'react'
import './ctrl-button.scss'

interface Props {
  content: string
  active: boolean
  click: Function
}

export class CtrlButton extends React.Component<Props, any>{
  render() {
    return (
      <button
        className={this.props.active ? 'ctrl-button-component active' : 'ctrl-button-component'}
        onClick={() => this.props.click()}
      >
        {this.props.content}
      </button>
    )
  }
}