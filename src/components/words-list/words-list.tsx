import React from 'react'
import {Words, Group, User} from '../../interfaces';
import {WordsTile} from './words-tile/words-tile';
import './words-list.scss'
import { ServiceContext } from '../../context'
import { GroupsService ,RouterService, WordsService, UserService } from '../../services';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
}

interface State {
  wordsList: Words[];
  selected: string;
  group: Group,
  favorite: string[]
}

export class WordsList extends React.Component<Props, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  routerService: RouterService = {} as RouterService
  wordsService: WordsService = {} as WordsService
  groupsService: GroupsService = {} as GroupsService 
  userService: UserService = {} as UserService

  destroy$: Subject<boolean> = new Subject<boolean>()

  state = {
    selected: '',
    wordsList: [],
    group: {} as Group,
    favorite: []
  } as State

  componentDidMount() {
    this.routerService = this.context.routerService
    this.wordsService = this.context.wordsService
    this.groupsService = this.context.groupsService
    this.userService = this.context.userService
    this.setState({
      selected: '',
      wordsList: []
    })
    this.groupsService.currentGroupSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((G: Group) => {
      this.setState({group: G})
      if (G?.vocabulary === undefined) {
        this.routerService.routeTo('/groups')
      }
    })
    this.wordsService.wordsSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((W: Words[]) => {
      this.setState({wordsList: W})
      if (W === undefined) {
        this.routerService.routeTo('/groups')
      }
    })
    this.userService.userSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((U: User) => {
      this.setState({favorite: U.favorite})
    })
  }

  componentWillUnmount() {
    this.destroy$.next(true)
    this.destroy$.unsubscribe()
  }

  editWords(w: Words) {
    this.wordsService.setCurrentWords(w)
    this.routerService.routeTo('/edit')
  }

  /** generate list of words from vocabylary */
  renderList = () => {
    if (this.state.wordsList === undefined || this.state.group?.vocabulary === undefined) {
      return <li key="unknown">LIST IS EMPTY</li>
    }
    
    return this.state.group.vocabulary.map((id: string, index: number) => {

      const W = this.state.wordsList.find((W: Words) => W.id === id)
      if (!W) {
        return <li key={`unknown${index}`}>unknown</li>
      }
      return (  <li
          key={`${W.name + index}`}
          className={W.name === this.state.selected ? 'words-list-tile selected' : 'words-list-tile'}
          onClick={() => { this.setState({ selected: W.name }) }}
        >
          <WordsTile
            setCurrentWords={() => this.wordsService.setCurrentWords(W)}
            editWords={() => this.editWords(W)}
            w={W}
            favorite={this.state.favorite}
          />
        </li>)
    })
  }

  render() {
    return (
      <div className="words-list">
        <ul>
          {this.renderList()}
        </ul>
      </div>
    )
  }
}