import React from 'react';
import {Words} from '../../../interfaces';
import './words-tile.scss';
import { IoDice } from 'react-icons/io5';
import { MdEdit } from 'react-icons/md';
import { AiFillStar } from 'react-icons/ai';
import {TileIcon} from '../../'
import {UserService, RouterService} from '../../../services'
import { ServiceContext } from '../../../context'

interface Props {
  setCurrentWords: Function;
  editWords: Function;
  w: Words
  favorite: string[]
}

export class WordsTile extends React.Component<Props, any> {
  static contextType = ServiceContext
  context!: React.ContextType<typeof ServiceContext>
  userService: UserService = {} as UserService
  routerService: RouterService = {} as RouterService

  componentDidMount() {
    this.userService = this.context.userService
    this.routerService = this.context.routerService
  }

  getIcon() {
    if (this.props.w.icon === '') {
      return this.props.w.words.length
    } else {
      return <TileIcon iconName={this.props.w.icon} />
    }
  }

  toggleFavorite(id: string) {
    this.userService.toggleFavorite(id).subscribe()
  }

  startGame() {
    this.props.setCurrentWords(this.props.w)
    this.routerService.routeTo('/game')
  }

  render() {
    const isFavorite = this.props.favorite.indexOf(this.props.w.id) >= 0 
    const favourite = isFavorite ? <AiFillStar /> : '' // has to change this later
    return (
      <React.Fragment>
        <div className="main-tile">
          <div className="words-count">
            {this.getIcon()}
          </div>
          <div className="words-info">
            <div className="words-name">{this.props.w.name}</div>
            <div className="words-group">{this.props.w.group}</div>
          </div>
          <div className="words-favourite">{favourite}</div>
        </div>

        <div className="secondary-tile">
          <div
            onClick={() => this.startGame()}><IoDice /></div>
          <div
            onClick={() => this.props.editWords(this.props.w)}
          ><MdEdit /></div>
          <div
            className={isFavorite ? 'favourite' : ''}
            onClick={() => {this.toggleFavorite(this.props.w.id)}}
          ><AiFillStar /></div>
        </div>
      </React.Fragment>
    )
  }
}