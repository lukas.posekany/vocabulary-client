import { FaCarrot, FaAppleAlt, FaDog, FaTshirt } from 'react-icons/fa'
import { BsFileEarmarkText } from 'react-icons/bs'
import { AiFillStar, AiOutlineNumber } from 'react-icons/ai'
import { GiHotMeal, GiPineapple, GiMeal, GiLion, GiMountaintop, GiPoliceOfficerHead, GiRainbowStar, GiSaltShaker, GiWoodenChair } from 'react-icons/gi'
import { HiDotsCircleHorizontal } from 'react-icons/hi'
import { MdFiberNew, MdKitchen } from 'react-icons/md'
import { RiRunFill, RiCalendarEventFill } from 'react-icons/ri'
import { WiDaySnowThunderstorm } from 'react-icons/wi'
import { IoHome, IoFastFood } from 'react-icons/io5'
import { GoTools } from 'react-icons/go'
import { GiLog } from 'react-icons/gi'
import React from 'react'


interface Props {
  iconName: string
}

// ATENTION!!!

// when new icon is added, it also has to be added into ./available-icons.ts

export class TileIcon extends React.Component<Props, any> {
  render() {
    switch (this.props.iconName) {
      case 'FaCarrot':
        return <FaCarrot />
      case 'AiFillStar':
        return <AiFillStar />
      case 'HiDotsCircleHorizontal':
        return <HiDotsCircleHorizontal />
      case 'FaAppleAlt':
        return <FaAppleAlt />
      case 'GiHotMeal':
        return <GiHotMeal />
      case 'GiMeal':
        return <GiMeal />
      case 'FaDog':
        return <FaDog />
      case 'GiLion':
        return <GiLion />
      case 'MdFiberNew':
        return <MdFiberNew />
      case 'RiRunFill':
        return <RiRunFill />
      case 'AiOutlineNumber':
        return <AiOutlineNumber />
      case 'GiMountaintop':
        return <GiMountaintop />
      case 'GiPoliceOfficerHead':
        return <GiPoliceOfficerHead />
      case 'RiCalendarEventFill':
        return <RiCalendarEventFill />
      case 'GiRainbowStar':
        return <GiRainbowStar />
      case 'GiPineapple':
        return <GiPineapple />
      case 'WiDaySnowThunderstorm':
        return <WiDaySnowThunderstorm />
      case 'GiSaltShaker':
        return <GiSaltShaker />
      case 'IoHome':
        return <IoHome />
      case 'MdKitchen':
        return <MdKitchen />
      case 'IoFastFood':
        return <IoFastFood />
      case 'GiWoodenChair':
        return <GiWoodenChair />
      case 'FaTshirt':
        return <FaTshirt />
      case 'GoTools':
        return <GoTools />
      case 'GiLog':
        return <GiLog />
      default:
        return <BsFileEarmarkText />
    }
  }
}