import React from 'react'
import './icons-picker.scss'
import { availableIcons } from '../tile-icons/available-icons'
import {TileIcon} from '../'

interface Props {
  selectedIcon: string
  selectIcon: Function
}

interface State {
  displayList: boolean
}

export class IconsPicker extends React.Component<Props, State> {

  state = {
    displayList: false
  }

  clickFunction: any;

  componentDidMount() {
    this.clickFunction = this.documentClick
    document.addEventListener("click", this.clickFunction);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.clickFunction);
  }

  documentClick = (e: Event) => {
    const target = e.target as HTMLTextAreaElement
    if (
      target?.id === 'all-icons' ||
      target?.parentElement?.id === 'all-icons' ||
      target?.id === 'current-icon' ||
      target?.parentElement?.id === 'current-icon' ||
      target?.parentElement?.parentElement?.id === 'current-icon' ||
      target?.parentElement?.parentElement?.parentElement?.id === 'current-icon'
    ) {
    } else {
      this.setState({ displayList: false })
    }
  }

  renderAllowedIcons() {
    return availableIcons.map((icon: string) => (
      <div
        key={icon}
        className={this.props.selectedIcon === icon ? 'icon-active' : ''}
        onClick={() => {
          this.setState({displayList: false})
          this.props.selectIcon(icon)
        } 
      }
      >
        <TileIcon iconName={icon} />
      </div>
    ))
  }


  render() {

    let list
    if (this.state.displayList) {
      list = (
        <div className="icons-wrapper">
          <div
            className="all-icons"
            id="all-icons">
            {this.renderAllowedIcons()}
          </div>
        </div>
      )
    }
    return (
      <div className="icons-picker-component">
        <div
          className="current-icon"
          id="current-icon"
          onClick={() => this.setState({ displayList: !this.state.displayList })}
        >
          <TileIcon
            iconName={this.props.selectedIcon}
          />
        </div>
        {list}

      </div>
    )
  }

}