import React from 'react';
import {Words} from '../../../interfaces';
import './genus.scss';

interface Props {
  currentWords: Words;
}

interface State {
  words: string[][];
  clickedGenus: string;
  locked: boolean;
  // currentIndex: number; // will be always 0 and I'll be just switching and removing from words array
}

export default class Genus extends React.Component<Props, State> {
  state = {
    words: this.props.currentWords.words,
    clickedGenus: '',
    locked: false,
  } as State

  /** split word to genus and rest, convert r, e, s to der, die, das 
   * @param word word in german to be splitted */
  splitWord = (word: string): string[] => {
    let [genus, content] = word.split(' ', 2)
    switch (genus) {
      case 'r':
        genus = 'der'
        break;
      case 'e':
        genus = 'die'
        break;
      case 's':
        genus = 'das'
        break;
      default:
        genus = ''
        break;
    }
    return [genus, content]
  }

  /** after user click one of genuses, check if he is correct 
   * @param genusCorrect correct genus of word
   * @param genusIn genus from user */
  checkWord = (genusCorrect: string, genusIn: string) => {
    this.setState({ clickedGenus: genusIn })
    if (genusCorrect === genusIn) {
      this.setState({ locked: true })
      setTimeout(() => {
        this.setState({
          words: this.state.words.slice(1, this.state.words.length),
          clickedGenus: '',
          locked: false
        })
      }, 700);
      return
    } else {

    }
  }

  /** get class name for li button 
   * @param buttonGenus the genus of button (li) I'm the className is for
   * @param correctGenus correct genus of word displayed to user */
  getClassName = (buttonGenus: string, correctGenus: string): string => {
    if (this.state.clickedGenus === '') {return ''}
    if (buttonGenus === this.state.clickedGenus) {
      if (this.state.clickedGenus === correctGenus) {
        return 'correct'
      } else {
        return 'incorrect'
      }
    } else {
      if (buttonGenus === correctGenus) {
        return 'blink'
      } else {
        return ''
      }
    }
  }

  render() {
    if(this.state?.words.length === 0) {
      return (<div>NO WORDS IN THIS SET</div>)
    }
    let [genus, content] = this.splitWord(this.state.words[0][1])
    return (
      <div className="game-genus">
        <div className={this.state.clickedGenus === '' ? 'word' : genus === this.state.clickedGenus ? 'correct word' : 'incorrect word'}>{content}</div>
        <ul>
          <li
            className={this.getClassName('der', genus)}
            onClick={() => this.checkWord(genus, 'der')}>der</li>
          <li
            className={this.getClassName('die', genus)}
            onClick={() => this.checkWord(genus, 'die')}>die</li>
          <li
            className={this.getClassName('das', genus)}
            onClick={() => this.checkWord(genus, 'das')}>das</li>
        </ul>
      </div>
    )
  }
}