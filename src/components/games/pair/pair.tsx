import React from 'react';
import './pair.scss';
import shuffleArr from '../../../lib'
import {Words} from '../../../interfaces';

interface State {
  currentWords: Words,
  selected: [string, string],
  wrong: [string, string],
  wordsDone: [string, string][],
  L0Col: string[],
  L1Col: string[]
}

interface Props {
  currentWords: Words
}

export default class Pair extends React.Component<Props, State> {
    state = {
        currentWords: this.props.currentWords,
        // currentWords: this.props.currentWords,

        /** current usets selection */
        selected: ['', ''],
        
        /** if user was wrong */
        wrong: ['', ''],

        /** all words done */
        wordsDone: [],
        L0Col: [],
        L1Col: []
    } as State

    componentDidMount = () => {
        this.initColumns()
    }

    /** set columns of words to defult state */
    initColumns = () => {
        let L0Col = this.props.currentWords.words.map(word => word[0])
        let L1Col = this.props.currentWords.words.map(word => word[1])

        L0Col = shuffleArr(L0Col)
        L1Col = shuffleArr(L1Col)

        this.setState({
            L0Col: L0Col,
            L1Col: L1Col,
            wordsDone: []
        })
    }

    /** when user selects word of column
     * @param {*} col 0 or 1 (left or right column)
     * @param {*} word word selected by user */
    select = (col: number, word: string) => {
        if(this.isDone(col, word)) {
            return;
        }
        let selected: [string, string] = this.state.selected
        let wrong: [string, string] = ['', '']
        selected[col] = selected[col] === word ? '' : word
        if (selected[0] !== '' && selected[1] !== '') {
            if (this.selectionCorrect(selected)) {
                this.selectedDone(selected)
                selected = ['', '']
            } else {
                wrong = selected
                selected = ['', '']
            }
        }
        this.setState({ selected: selected, wrong: wrong })
    }

    /** check if user was correct
     * @param {*} selected two selected words from oposite columns */
    selectionCorrect = (selected: [string, string]) => {
        const correct = this.state.currentWords.words.find(
            word => word[0] === selected[0]
        )
        if (!correct) {
          return false
        }
        if (correct[1] === selected[1]) {
            return true
        } else {
            return false
        }
    }

    /** remove selected word from columns
     * @param {*} selected words from col1 and col2 */
    selectedDone = (selected: [string, string]) => {
        this.setState({ wordsDone: [...this.state.wordsDone, selected]})
        setTimeout(() => {
            if ( this.state.L0Col.length === this.state.wordsDone.length ) {
                this.initColumns()
            }
        }, 2000)
    }

    /** get class for word tile indicating state of user action 
     * @param {*} col which column user clicked
     * @param {*} word the word of column user clicker */
    getClass = (col: number, word: string) => {
        let classes = ''
        if (this.isDone(col, word)) {
            return 'done'
        }
        if (this.state.selected[col] === word) {
            classes += 'selected'
        } else {
            if (this.state.wrong[col] === word) { // wrong
                classes += 'wrong'
            }
        }
        return classes
    }

    /** check if word is in wordsDone
     * @param {*} col column 0, 1 to which word belong
     * @param {*} word selected word */
    isDone(col: number, word: string) {
        let found = this.state.wordsDone.find(wd => wd[col] === word)
        if (found) {
            return true
        } else {
            return false
        }
    }

    renderColumns = () => {
        const Col1 = this.state.L0Col.map((w: string, i: number) =>
            <li key={`col1: ${w + i}`}
                className={this.getClass(0, w)}
                onClick={() => this.select(0, w)}>
                <button>{w}</button>
            </li>
        )
        const Col2 = this.state.L1Col.map((w: string, i: number) =>
            <li key={`col2: ${w + i}`}
                className={this.getClass(1, w)}
                onClick={() => this.select(1, w)}>
                <button>{w}</button>
            </li>
        )
        return [Col1, Col2]
    }

    render() {
      if(this.state?.currentWords.words.length === 0) {
        return (<div>NO WORDS IN THIS SET</div>)
      }
        let [Col1, Col2] = this.renderColumns()
        return (
            <div className="game-connect">
                <ul>{Col1}</ul>
                <ul>{Col2}</ul>
            </div>
        )
    }
}