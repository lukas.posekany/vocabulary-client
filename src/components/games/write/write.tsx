import React from 'react'
import {Words} from '../../../interfaces';
import { RiArrowUpDownLine } from 'react-icons/ri'
import './write.scss'

interface Props {
  currentWords: Words
}

interface State {
  maxRepeat: number;
  inputWord: string;
  wordIndex: number;
  currentWordsSet: string[][];
  languagesOrder: [number, number];
  languages: string[];
  wrong: boolean;
}

export default class Write extends React.Component<Props, State> {
  state = {
    maxRepeat: 4,
    inputWord: '',
    wordIndex: 0,
    currentWordsSet: this.props.currentWords.words,
    languages: this.props.currentWords.languages,
    languagesOrder: [0, 1],
    wrong: false
  } as State;

  /** process user input inside text input element */
  userInput = (event: any) => {
    this.setState({ inputWord: event.target.value })
  };

  /** reorder words randomly */
  shuffleWords = () => {
    for (let word of this.state.currentWordsSet) {
      console.log(word)
    }
  }

  addWrongAnswer(word: string[]) {
    let newArray = JSON.parse(JSON.stringify(this.state.currentWordsSet))
    let i = Math.ceil(Math.random() * 2) + 1
    if (i < newArray.length) {
      newArray.splice(i, 0, word)
    } else {
      newArray.push(word, word)
      return
    }
    i = Math.ceil(Math.random() * 2) + i
    if (i < newArray.length) {
      newArray.splice(i, 0, word);
    } else {
      newArray.push(word)
    }
    this.setState({ currentWordsSet: newArray });
  }

  /** process users submission of form */
  checkWord = (event: any) => {
    event.preventDefault();
    const guessedLang: number = this.state.languagesOrder[1]
    const correctWord = this.state.currentWordsSet[this.state.wordIndex][guessedLang]

    if (this.state.inputWord === correctWord) { // player was correct
      this.setState({ wrong: false })
      const withoutFirst = this.state.currentWordsSet.slice(1)
      if (withoutFirst.length === 0) {
        alert('Congratulations, you won.\nNEW ROUND IS HERE')
        this.setState({ currentWordsSet: this.props.currentWords.words })
      } else {
        this.setState({ currentWordsSet: withoutFirst })
      }
    } else { // player was incorrect
      this.addWrongAnswer(this.state.currentWordsSet[this.state.wordIndex])
      this.setState({ wrong: true })
    }
    this.setState({ inputWord: "" })
  };

  /** swap languages */
  toggleLanguages = () => {
    this.setState({ languagesOrder: [this.state.languagesOrder[1], this.state.languagesOrder[0]] })
  };

  render() {
    if(this.state?.currentWordsSet.length === 0) {
      return (<div>NO WORDS IN THIS SET</div>)
    }
    return (
      <form className="write-game" onSubmit={this.checkWord} action="">
        <label className={this.state.wrong ? 'word incorrect' : 'word'}>
          {this.state.currentWordsSet[this.state.wordIndex][this.state.languagesOrder[0]]}
        </label>
        <div className="switch-languages">
          <RiArrowUpDownLine
            onClick={this.toggleLanguages} />
        </div>
        <label className="input-word">
          <input
            type="text"
            value={this.state.inputWord}
            onChange={this.userInput}
            autoCapitalize="off"
            autoComplete="off"
            spellCheck="false"
            autoCorrect="off"
          />
        </label>
        <label className="hint">
          {this.state.wrong ? this.state.currentWordsSet[this.state.wordIndex][this.state.languagesOrder[1]] : ''}
        </label>
        {/* <button type="submit">Check</button> */}
      </form>
    )
  }

}