import React from 'react'
import './game.scss'
import Write from './write/write'
import Pair from './pair/pair'
import {  Words } from '../../interfaces'
import Genus from './genus/genus'
import { ServiceContext } from '../../context'
import { RouterService, WordsService } from '../../services'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { CtrlBar, ctrlElement } from '..'

interface Props {
}

interface State {
  currentWords: Words;
  currentGame: string;
}

export class Game extends React.Component<Props, State> {
  static contextType = ServiceContext;
  context!: React.ContextType<typeof ServiceContext>;
  routerService: RouterService = {} as RouterService
  wordsService: WordsService = {} as WordsService

  destroy$: Subject<boolean> = new Subject<boolean>()

  state = {
    currentWords: {} as Words,
    currentGame: 'pair'
  }

  ctrlBar: ctrlElement[] = [
    {
      text: "write",
      emit: () => this.setState({ currentGame: 'write' })
    }, {
      text: "pair",
      emit: () => this.setState({ currentGame: 'pair' })
    }, {
      text: "genus",
      emit: () => this.setState({ currentGame: 'genus' })
    }
  ]

  componentDidMount() {
    this.routerService = this.context.routerService
    this.wordsService = this.context.wordsService

    this.wordsService.currentWordsSubject
      .pipe(takeUntil(this.destroy$))
      .subscribe((W: Words) => {
        this.setState({
          currentWords: W
        })
      })
  }

  componentWillUnmount() {
    this.destroy$.next(true)
    this.destroy$.unsubscribe()
  }

  render() {

    if (!this.state.currentWords?.words || this.state.currentWords?.words.length <= 0) {
      return <div>NOT WORDS SELECTED OR WORDS ARE EMPTY</div>
    }

    let game;
    switch (this.state.currentGame) {
      case 'write':
        game = <Write
          currentWords={JSON.parse(JSON.stringify(this.state.currentWords))}
        />
        break;
      case 'pair':
        game = <Pair
          currentWords={JSON.parse(JSON.stringify(this.state.currentWords))}
        />
        break;
      case 'genus':
        game = <Genus
          currentWords={JSON.parse(JSON.stringify(this.state.currentWords))}
        />
        break;
      default:
        game = <h1>NOT GAME SELECTED</h1>
    }

    return (
      <div className="game">
        <div className="game-window">
          <CtrlBar
            elements={this.ctrlBar}
            activeElement={this.state.currentGame}
          />
          <span></span>
          {game}
        </div>
      </div>
    )
  }
}