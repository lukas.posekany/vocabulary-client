import React from 'react'
import './App.scss';
import { Router } from 'react-router';
import { Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from "history";
import { RouteGuard, AdminGuard } from './routeGuard';
import { Game, WordsList, GroupsList, TopNav, EditWords, Login, Home, EditUser} from './components'
import { ServiceContext, servicesGroup } from './context'
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const history = createBrowserHistory()

interface State {
  currentGame: string,
}

export default class App extends React.Component<any, State> {

  destroy$: Subject<boolean> = new Subject<boolean>()

  componentDidMount() {
    servicesGroup.routerService.routeSubject
    .pipe(takeUntil(this.destroy$))
    .subscribe((route: string) => {
      history.push(route)
    })
  }

  componentWillUnmount() {
    this.destroy$.next(true)
    this.destroy$.unsubscribe()
  }

  state = {
    currentGame: 'pair'
  } as State

  render() {
    return (
      <ServiceContext.Provider value={servicesGroup}>
        <div className="App" >
          <Router history={history}>
            <div className="content">
              <Switch>
                <Route exact path="/login"><Login /></Route>
                <RouteGuard component={WordsList} path="/words" />
                <RouteGuard component={GroupsList} path="/groups" />
                <RouteGuard component={Game} path="/game" />
                <RouteGuard component={EditWords} path="/edit" />
                <AdminGuard component={EditUser} path="/editUser" />
                {/* THIS MUST BE AT THE END, OTHERWISE IT WILL CATCH ALL */}
                <RouteGuard component={Home} path="/" />
              </Switch>
            </div>
            <div className="top-menu-div">
              <TopNav />
            </div>

{/* I'll left this here, because it will make padding on bottom of page for that anoying controls of mobile browsers */}
            <div className="bottom-menu-div">
            </div>

          </Router>
        </div>
      </ServiceContext.Provider>
    );
  }
}