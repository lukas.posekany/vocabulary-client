export interface Group {
  id: string,
  name: string,
  icon: string,
  vocabulary: string[]
}