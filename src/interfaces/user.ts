export interface User {
  id: string,
  name: string,
  role: string,
  email: string,
  password: string,
  words: string[],
  groups: string[],
  favorite: string[],
  favoriteGroups: string[],
}