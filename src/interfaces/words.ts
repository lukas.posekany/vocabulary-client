export interface Words {
  id: string,
  name: string,
  group: string,
  icon: string,
  allowedGames: string[],
  languages: [string, string],
  words: string[][]
}