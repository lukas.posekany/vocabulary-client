export default function shuffleArr(arr) {
    for (let i = 0; i < arr.length; i++) {
        const ran = Math.floor(Math.random() * arr.length)
        const c = arr[ran]
        arr[ran] = arr[i]
        arr[i] = c
    }
    return arr
}