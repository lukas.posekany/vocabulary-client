# BUILD
FROM node:latest as node

WORKDIR /app

COPY . .

RUN npm install

RUN npm audit fix

RUN npm run build

# DEPLOY

FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/conf.d/

RUN rm -rf /usr/share/nginx/html/*

COPY --from=node /app/build /usr/share/nginx/html